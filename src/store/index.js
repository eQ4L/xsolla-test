import Vue from 'vue'
import Vuex from 'vuex'

import transactions from './modules/transactions'
import headers from './modules/headers'
import projects from './modules/projects'
import paymentRate from './modules/paymentRate'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { headers, transactions, projects, paymentRate }
})
