import data from '@/data'

const state = {
  transactions: [],
  filteredTransactions: [],
  filter: ''
}

const getters = {
  transactions: state => state.transactions,
  filteredTransactions: state => state.filteredTransactions,
  filter: state => state.filter
}

const mutations = {
  setTransactions (state, transactions) {
    state.transactions = transactions
    state.filteredTransactions = transactions
  },
  setFilter (state, filter) {
    state.filter = filter.toLowerCase()
    state.filteredTransactions = state.transactions.filter(item => {
      return item.transaction.project.name.toLowerCase().includes(filter) ||
      item.transaction.payment_method.name.toLowerCase().includes(filter) ||
      item.payment_details.payment.currency.toLowerCase().includes(filter)
    })
  }
}

const actions = {
  setTransactions ({state, commit}) {
    commit('setTransactions', data)
  },
  applyFilter ({state, commit}, filter) {
    commit('setFilter', filter)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
