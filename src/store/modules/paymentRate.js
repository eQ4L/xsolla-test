const state = {
  paymentRate: []
}

const getters = {
  paymentRate: state => state.paymentRate
}

const mutations = {
  countPaymentRate (state, paymentRate) {
    state.paymentRate = paymentRate
  }
}

const actions = {
  countPaymentRate ({ commit, rootState }) {
    const paymentCounter = rootState.transactions.transactions.reduce((obj, item) => {
      obj[item.transaction.payment_method.name] = (obj[item.transaction.payment_method.name] || 0) + 1
      return obj
    }, {})

    const counter = Object.keys(paymentCounter).map(item => {
      return {
        name: item,
        count: paymentCounter[item]
      }
    })

    const sortedCounter = counter.sort((a, b) => a.count < b.count)
    commit('countPaymentRate', sortedCounter)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
