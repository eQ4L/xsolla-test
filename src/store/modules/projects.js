const state = {
  projectsCounter: {}
}

const getters = {
  projectsCounter: state => state.projectsCounter
}

const mutations = {
  countProjects (state, projectsCounter) {
    state.projectsCounter = projectsCounter
  }
}

const actions = {
  countProjects ({ commit, rootState }) {
    const projectsCounter = rootState.transactions.transactions.reduce((obj, item) => {
      obj[item.transaction.project.name] = (obj[item.transaction.project.name] || 0) + 1
      return obj
    }, {})

    const counter = Object.keys(projectsCounter).map(item => {
      return {
        name: item,
        count: projectsCounter[item]
      }
    })
    commit('countProjects', counter)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
