const state = {
  transactionsHeaders: [
    { text: 'Transaction_id', value: 'Transaction_id', sortable: false },
    { text: 'Status', value: 'Status', sortable: false },
    { text: 'Date', value: 'Date', sortable: false },
    { text: 'Payment_method', value: 'Payment_method', sortable: false },
    { text: 'Project', value: 'Project', sortable: false },
    { text: 'Payment_amount', value: 'Payment_amount', sortable: false },
    { text: 'Payment_currency', value: 'Payment_currency', sortable: false }
  ],
  projectsHeaders: [
    { text: 'Project', value: 'Project', sortable: false },
    { text: 'Count', value: 'Count', sortable: false }
  ],
  paymentRateHeaders: [
    { text: 'Payment Type', value: 'Payment Type', sortable: false },
    { text: 'Count', value: 'Count', sortable: false }
  ]
}

const getters = {
  transactionsHeaders: state => state.transactionsHeaders,
  projectsHeaders: state => state.projectsHeaders,
  paymentRateHeaders: state => state.paymentRateHeaders
}

export default {
  namespaced: true,
  state,
  getters
}
