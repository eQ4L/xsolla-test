import Vue from 'vue'
import Router from 'vue-router'
import Transactions from '@/pages/Transactions'
import Projects from '@/pages/Projects'
import PaymentRate from '@/pages/PaymentRate'
import Chart from '@/pages/Chart'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/transactions',
      name: 'Transactions',
      component: Transactions
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects
    },
    {
      path: '/rate',
      name: 'PaymentRate',
      component: PaymentRate
    },
    {
      path: '/chart',
      name: 'Chart',
      component: Chart
    },
    {
      path: '*',
      redirect: '/transactions'
    }
  ]
})
