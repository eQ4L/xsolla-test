# xsolla-test

> A test task for xsolla summer school 2018 frontend flow

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

```

## [DEMO](http://eqprod.ru:3100)
